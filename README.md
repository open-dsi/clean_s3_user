# Clean S3 user
Properly clean a Nextcloud user with S3 storage

## Installation
```bash
apt install libmariadb3 libmariadb-dev
pip3 install -r requirements.txt
```

## Usage
* **On the Nextcloud host**
```bash
# One user immediate deletion
python3 clean_s3_user.py -p /opt/nextcloud -u test@sitiv.fr
# One user with delayed S3 deletion
python3 clean_s3_user.py -p /opt/nextcloud -u test@sitiv.fr -b cache.csv
# One user transfer
python3 clean_s3_user.py -p /opt/nextcloud -u test@sitiv.fr -d admin@sitiv.fr
# Mass user deletion with exception
python3 clean_s3_user.py -p /opt/nextcloud --all -e odsiadmin -e supervision -e watcha
# Enable debug
python3 clean_s3_user.py -p /opt/nextcloud [...] -v
# Save log to file
python3 clean_s3_user.py -p /opt/nextcloud [...] -l clean.log
# Full example
python3  clean_s3_user.py -p /opt/nextcloud --all -e odsiadmin -e supervision -e watcha -l clean.log -v
```
